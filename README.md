TV show watch list
========================

A TV show app for receiving news about your favourite TV shows and receiving notification on new episodes.
Rate TV shows, add to different watch lists, check the seen episodes.
